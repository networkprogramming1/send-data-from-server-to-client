#หากต้องการใช้ angsila จำเป็นต้องเปลี่ยน HOST เป็น angsila.cs.buu.ac.th
#แต่ไม่สามารถใช้ port 22 ของ angsila ได้ เนื่องจากไม่มีสิทธิ์ เลยจำเป็นต้อง test เป็น server ตัวเอง
import socket
HOST = 'localhost'
PORT = 1711
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(5)
while True:
    print("Waiting for connection")
    connection, client_address = s.accept()
    try:
        print("connection from", client_address)
        file_name = connection.recv(1024).decode('utf-8')
        with open(file_name,'rb') as file:
            file_contents = file.read()
        connection.sendall(file_contents)
    finally:
        connection.close()
        print("Closed connection")