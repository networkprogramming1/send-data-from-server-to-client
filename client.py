import socket
HOST = 'localhost'
PORT = 1711
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
s.sendall(b'a.txt')
data = s.recv(1024)
file_contents = b""

while data:
    file_contents += data
    data = s.recv(1024)

s.close()

print('received data from server: ')
print(file_contents.decode('utf-8'))

